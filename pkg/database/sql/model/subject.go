package model

import (
	"github.com/jinzhu/gorm"
)

type Subject struct {
	gorm.Model
	Course       string `json:"course"`
	Code         string `json:"code"`
	Name         string `json:"name"`
	Type         string `json:"type"`
	Credits      string `json:"credits"`
	Period       string `json:"period"`
	Availability string `json:"availability"`
	Limit        string `json:"limit"`
	Language     string `json:"language"`
}