package model

import (
	"github.com/jinzhu/gorm"
)

type School struct {
	gorm.Model
	Ref        int    `json:"ref"`
	Name       string `json:"name"`
	University int    `json:"university"`
	Province   string `json:"province"`
	City       string `json:"city"`
	Address    string `json:"address"`
	Number     int    `json:"number"`
	PostalCode int    `json:"postal_code"`
}