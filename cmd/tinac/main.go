// Copyright (C) 2018  TiNAC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/tinac/tinac/pkg/database/sql/model"
	"gitlab.com/tinac/tinac/cmd/tinac/commands"
)

func main() {

	commands.Execute()

	db, err := gorm.Open("sqlite3", "test.sqlite3")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.AutoMigrate(&model.University{})

	university := model.University{
		Ref:          1,
		Established:  1999,
		Headquarters: "Zaragoza",
		Name:         "UNIZAR",
		Region:       "Aragón",
		Type:         "Publica",
	}

	db.Create(&university)
}
