package model

import (
	"github.com/jinzhu/gorm"
)

type Degree struct {
	gorm.Model
	Ref    int    `json:"ref"`
	Name   string `json:"name"`
	Code   string `json:"code"`
	School int    `json:"school"`
}