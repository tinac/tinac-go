package model

import (
	"github.com/jinzhu/gorm"
)

type University struct {
	gorm.Model
	Ref          int    `json:"ref"`
	Name         string `json:"name"`
	Headquarters string `json:"headquarters"`
	Region       string `json:"region"`
	Established  int    `json:"established"`
	Type         string `json:"type"`
}


func (U *University) Save(){

}