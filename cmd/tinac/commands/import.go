// Copyright (C) 2018  TiNAC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"io/ioutil"
	"os"
	"gitlab.com/tinac/tinac/pkg/database/sql/model"
	"encoding/json"
)

// flag values
var import_type string
var entity_type string

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import [flags] [file to import]",
	Short: "Import data into the tinac database",
	Long: `This command let's you to import data into the tinac database`,
	Args: cobra.ExactArgs(1),	// This command will require one arg, the filepath
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("import called")
		fmt.Println("File path:",args[0])
		importData(args[0])
	},
}

func init() {
	rootCmd.AddCommand(importCmd)

	// Here you will define your flags and configuration settings.
	importCmd.Flags().StringVarP(&import_type, "type", "t","json", "File type <json, cvs>")
	importCmd.Flags().StringVarP(&entity_type, "entity", "e","", "Type of entity that that the file contains")
}

func importData(filePath string){
	// Open our jsonFile
	jsonFile, err := os.Open(filePath)

	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we initialize our School array
	var schools []model.School

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'schools' which we defined above
	json.Unmarshal(byteValue, &schools)

	for _, school := range schools {
		fmt.Println(school)
	}
}
